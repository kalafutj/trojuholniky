#include "nacitavanie.h"
#include "mnozina.h"
#include <iostream>
#include <string>

using namespace std;

int main() {

	string subor;

	cout << "Zadajte nazov subor so zadanymi suradnicami trojuhlnikov" << endl;
	cin >> subor;
	nacitavanie N(subor);

	mnozina M = N.citaj();

	cout << endl;
	cout << "pocet ostrouhlych trojuholnikov:" << M.ostry() << endl;
	cout << "pocet pravouhlych trojuholnikov:" << M.pravy() << endl;
	cout << "pocet tupouhlych trojuholnikov:" << M.tupy() << endl;
	cout << endl;
	cout << "pocet rovnostrannych trojuholnikov:" << M.rovnostranny() << endl;
	cout << "pocet rovnoramennych trojuholnikov:" << M.rovnoramenny() << endl;
	cout << endl;
	M.udaje();
	cout << endl;
	M.prienik();
	cout << endl;
	M.podoba();
	cout << endl;


	system("PAUSE");
	
}