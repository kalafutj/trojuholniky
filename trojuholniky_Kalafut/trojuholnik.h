#pragma once
#include "vektor.h"
#include <iostream>

class trojuholnik
{
	friend class mnozina;
public:
	trojuholnik();
	trojuholnik(vektor, vektor, vektor);
	~trojuholnik();


private:
	bool rovnostranny();
	bool rovnoramenny();
	void nastav(vektor,vektor,vektor);
	double obvod();
	double obsah();
	vektor BOD[3];
	double uhol[3];
	vektor V[3];
	bool kolinearny=false;
	bool rovnost(double, double=0);
};

