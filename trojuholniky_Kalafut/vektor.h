#pragma once
#include <cmath>

class vektor
{
public:
	vektor();
	vektor(double,double);
	~vektor();
	double operator* (vektor);
	vektor operator* (int);
	vektor operator- (vektor);
	double dlzka();
	double operator/ (vektor);

private:
	double x, y;
};

