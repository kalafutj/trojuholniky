#pragma once
#include "vektor.h"
#include <vector>
#include "mnozina.h"
#include <string>
#include <fstream>

class nacitavanie
{
public:
	nacitavanie(std::string);
	mnozina citaj();
	~nacitavanie();
private:
	std::fstream subor;
};

