#include "vektor.h"



vektor::vektor()
{
}

vektor::vektor(double X, double Y)
{
	x = X;
	y = Y;
}

//skalarny sucin
double vektor::operator*(vektor v) {

	return (x*v.x + y*v.y);

}

//nasobok skalarom
vektor vektor::operator*(int t) {
	vektor v(t*x, t*y);
	return (v);
}

// odcitanie vektorov
vektor vektor::operator-(vektor v) {
	vektor w(x - v.x, y - v.y);
	return (w);
}

double vektor::dlzka()
{
	return std::sqrt((*this)*(*this));
}

//vektorovy sucin
double vektor::operator/(vektor v)
{
	return (x*v.y - y*v.x);
}

vektor::~vektor()
{
}
