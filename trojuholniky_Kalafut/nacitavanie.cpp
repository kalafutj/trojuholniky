#include "nacitavanie.h"



nacitavanie::nacitavanie(std::string meno)
{

	subor.open(meno, std::fstream::in);

}

mnozina nacitavanie::citaj() {
	std::vector<std::vector<vektor>> V;
	std::vector<vektor> W;
	double x, y;

	if (!subor.is_open())
		return (mnozina(V));

	while (!subor.eof()) {
		for (int i = 0;i < 3;i++) {
			subor >> x;
			subor >> y;
			W.push_back(vektor(x, y));
		}
		V.push_back(W);
		W.clear();
	}

	return (mnozina(V));
}

nacitavanie::~nacitavanie()
{
	subor.close();
}
