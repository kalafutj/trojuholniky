#include "trojuholnik.h"



trojuholnik::trojuholnik()
{
}

trojuholnik::trojuholnik(vektor A, vektor B, vektor C) {
	nastav(A, B, C);
}

void trojuholnik::nastav(vektor A,vektor B, vektor C) {
	BOD[0] = A;
	BOD[1] = B;
	BOD[2] = C;
	for (int i = 0;i < 3;i++)
		V[i] = BOD[(i + 1) % 3] - BOD[i];

	//test je na nulovy determinant
	if (rovnost(V[0] / V[1])) kolinearny = true;

	for (int i = 0;i < 3;i++) 
		uhol[i] = V[i] * (-1) * V[(i - 1) % 3];
	
}

bool trojuholnik::rovnostranny()
{
	if (rovnost(V[0].dlzka(),V[1].dlzka()))
		if (rovnost(V[0].dlzka(),V[2].dlzka()))
			return true;
	return false;
}

bool trojuholnik::rovnoramenny()
{
	if (rovnost(V[0].dlzka(),V[1].dlzka()))
		return true;
	if (rovnost(V[0].dlzka(),V[2].dlzka()))
		return true;
	if (rovnost(V[1].dlzka(),V[2].dlzka()))
		return true;
	return false;
}

double trojuholnik::obvod()
{
	double o = 0;
	for (int i = 0;i < 3;i++)
		o += V[i].dlzka();
	return o;
}

//Vychadzam z vektoroveho sucinu
double trojuholnik::obsah()
{
	return (abs(V[0]/V[1])/2);
}

trojuholnik::~trojuholnik()
{
}

//funkcia na porovnavanie (double sa nevyplati porovnavat na rovnost)
bool trojuholnik::rovnost(double a,double b) {

	a -= b;
	return (abs(a) < 0.0001);
}