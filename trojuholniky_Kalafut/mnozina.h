#pragma once
#include "trojuholnik.h"
#include "vektor.h"
#include <vector>

class mnozina
{
public:
	mnozina();
	mnozina(std::vector<std::vector<vektor>>);
	~mnozina();
	int rovnostranny();
	int rovnoramenny();
	int tupy();
	int ostry();
	int pravy();
	void prienik();
	void udaje();
	void podoba();
private:
	int n;
	trojuholnik *T;
	vektor gauss(vektor,vektor,vektor);
	bool podoba(int, int);
	bool prienik(int, int);
};