#include "mnozina.h"



mnozina::mnozina()
{
}

mnozina::mnozina(std::vector<std::vector<vektor>> zoz)
{
	n=zoz.size();
	T = new trojuholnik[n];
	if (T != 0)
		for (int i = 0;i < n;i++) 
			T[i].nastav(zoz[i][0], zoz[i][1], zoz[i][2]);

	std::cout << "Pocet trojuholnikov je " << n << std::endl;
}


mnozina::~mnozina()
{
	delete[] T;
}

int mnozina::rovnostranny()
{
	int pocet=0;
	for (int i = 0;i < n;i++) 
		if (!T[i].kolinearny&&T[i].rovnostranny())
			pocet++;
	return pocet;
}

int mnozina::rovnoramenny()
{
	int pocet = 0;
	for (int i = 0;i < n;i++)
		if (!T[i].kolinearny&&T[i].rovnoramenny())
			if (!T[i].rovnostranny())
				pocet++;
	return pocet;
}

int mnozina::tupy(){
	int pocet = 0;

	for (int i = 0;i < n;i++) {
		if (T[i].kolinearny) continue;
		for (int j = 0;j < 3;j++)
			if (T[i].uhol[j] < 0) {
				pocet++;
				break;
			}
	}
	return pocet;
}

int mnozina::pravy() {
	int pocet = 0;

	for (int i = 0;i < n;i++) {
		if (T[i].kolinearny) continue;
		for (int j = 0;j < 3;j++)
			if (!T[i].uhol[j]) {
				pocet++;
				break;
			}
	}
	return pocet;
}

int mnozina::ostry() {
	int pocet = n;

	for (int i=0;i<n;i++)
		for(int j=0;j<3;j++)
			if (T[i].uhol[j] <= 0) {
				pocet--;
				break;
			}

	return pocet;
}

bool mnozina::prienik(int T1, int T2)
{
	vektor C;
	vektor X(1, 0);
	vektor Y(0, 1);

	for (int i = 0;i < 3;i++) 
		for (int j = 0;j < 3;j++) {
			C = gauss(T[T1].V[i], T[T2].V[j] * -1, T[T2].BOD[j] - T[T1].BOD[i]);
			if (C*X>=0 && C*Y>=0)
				if (C*Y<=1 && C*Y<=1) return true;
		}

	for (int j = 0;j < 3;j++) {
		C = gauss(T[T1].V[1]*-1, T[T1].V[2], T[T2].BOD[j] - T[T1].BOD[2]);
		if (C*X >= 0 && C*Y >= 0)
			if (C*Y <= 1 && C*Y <= 1) 
				if (C*X<=1-C*Y) return true;
	}
	
	return false;
}

vektor mnozina::gauss(vektor u, vektor v, vektor b)
{
	double det = u / v;
	if (T->rovnost(det) && T->rovnost(b/v)  && T->rovnost(u / b))
		return (vektor(0, 0));
	return (vektor(b / v / det, u / b / det));
}

bool mnozina::podoba(int T1, int T2) {
	bool uuu = false;
	if (T[T1].kolinearny || T[T2].kolinearny)return false;
	double sss = T[T1].obvod() / T[T2].obvod();
	for (int i = 0;i < 3;i++) {
		for (int j = 0;j < 3;j++)
			if (T->rovnost(T[T1].V[i].dlzka()/T[T2].V[j].dlzka(), sss)) {
				uuu = true;
				break;
				}
		if (!uuu) return false;
		uuu = false;
	}
	return true;
}

void mnozina::prienik() {

	std::cout << "nasledujuce dvojice trojuholnikov sa pretinaju:" << std::endl;
	for (int i = 0; i < n;i++) {
		for (int j = i + 1;j < n;j++) {
			if (prienik(i, j)) std::cout << "[#" << i + 1 << ",#" << j + 1 << "] ";
		}
	}
	std::cout << std::endl;
}

void mnozina::udaje() {
	for (int i = 0;i < n;i++) {
		if (T[i].kolinearny) continue;
		std::cout << "Trojuholnik #" << i + 1 << " ma obsah: " << T[i].obsah();
		std::cout << " a obvod: " << T[i].obvod() << std::endl;
	}
}

void mnozina::podoba() {
	bool* prejdene = new bool [n];

	if (prejdene != 0) {

		for (int i = 0; i < n; i++) prejdene[i] = false;
		std::cout << "Nasledujuce mnoziny obsahuju navzajom podobne trojuholniky" << std::endl;

		for (int i = 0;i < n;i++) {
			if (prejdene[i]) continue;
			for (int j = i + 1;j < n;j++) {
				if (podoba(i, j)) {
					if (!prejdene[i]) {
						std::cout << "{" << i + 1;
						prejdene[i] = true;
					}
					std::cout << "," << j + 1;
					prejdene[j] = true;
				}
			}
			if (prejdene[i]) std::cout << "}" << std::endl;
		}

	}
	delete[] prejdene;

}